# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.10.2] - 2019-04-15

### Changed

- Add color to logos

## [0.10.1] - 2019-04-15

### Fixed

- Does not add `autoplay_next` parameter in URL.

## [0.10.0] - 2019-04-15

### Added

- Add an option to autoplay the next videos when the box is checked on Invidious.

### Changed

- Interface language is now set up for all webpages of Invidious.

## [0.9.1] - 2019-04-11

### Fixed

- Fix issue where a non-supported interface language was reloading the page forever.
- Set english as default interface language now.

## [0.9.0] - 2019-04-11

### Added

- Add interface language selection.

## [0.8.1] - 2019-04-10

### Changed

- Don't use innerHTML anymore

## [0.8.0] - 2019-04-10

### Added

- Updated uMatrix recipe
- Internationalization support! Supported languages are english and french.

### Changed

- List subtitles by native name

## [0.7.3] - 2019-04-09

### Added

- Updated uMatrix recipe to support known Invidious instances

### Fixed

- Don't force HTTPS for .onion domains
- Move 64x64 icons in the right directory.

## [0.7.2] - 2019-04-07

### Fixed

- Remove forgotten console.log()
- Fix issue with instance not being updated correctly.

## [0.7.1] - 2019-04-06

### Fixed

- No longer redirect instance subdomains to the main domain

## [0.7.0] - 2019-04-06

### Added

- Add "Force Proxy" option
- Add "Autoplay" option
- Add "Default Captions" option
- Apply selected options even when using direct Invidious links.

### Changed

- Rework with Typescript and use Webpack

## [0.6.0] - 2019-04-05

### Added

- Add 64x64 logos
- Add support for youtu.be

## [0.5.0] - 2019-04-01### Added

- Add README.md
- Add logo
- Add toolbar button to toggle addon activation
- Add uMatrix rules and recipes

## [0.4.0] - 2019-03-31

### Added

- Add CHANGELOG.md
- Now videos are proxied by using "local=true" parameter. No more Google requests.

## [0.3.0] - 2019-03-31

### Added

- Add support for youtube-nocookie.com

## [0.2.1] - 2019-03-31

### Fixed

- Add default value for instance setting
- Force usage of URL as instance setting to avoid issues

## [0.2.0] - 2019-03-31

### Added

- It is now possible to select which instance use

## [0.1.0] - 2019-03-31

### Added

- Initial release

[0.10.2]: https://gitlab.com/booteille/invidition/compare/v0.10.1...0.10.2
[0.10.1]: https://gitlab.com/booteille/invidition/compare/v0.10.0...0.10.1
[0.10.0]: https://gitlab.com/booteille/invidition/compare/v0.9.1...0.10.0
[0.9.1]: https://gitlab.com/booteille/invidition/compare/v0.9.0...0.9.1
[0.9.0]: https://gitlab.com/booteille/invidition/compare/v0.8.1...0.9.0
[0.8.1]: https://gitlab.com/booteille/invidition/compare/v0.8.0...0.8.1
[0.8.0]: https://gitlab.com/booteille/invidition/compare/v0.7.3...0.8.0
[0.7.3]: https://gitlab.com/booteille/invidition/compare/v0.7.2...0.7.3
[0.7.2]: https://gitlab.com/booteille/invidition/compare/v0.7.1...0.7.2
[0.7.1]: https://gitlab.com/booteille/invidition/compare/v0.7.0...0.7.1
[0.7.0]: https://gitlab.com/booteille/invidition/compare/v0.6.0...0.7.0
[0.6.0]: https://gitlab.com/booteille/invidition/compare/v0.5.0...0.6.0
[0.5.0]: https://gitlab.com/booteille/invidition/compare/v0.4.0...0.5.0
[0.4.0]: https://gitlab.com/booteille/invidition/compare/v0.3.0...0.4.0
[0.3.0]: https://gitlab.com/booteille/invidition/compare/ecbbf514...v0.3.0
[0.2.1]: https://gitlab.com/booteille/invidition/compare/c72a71bc...ecbbf514
[0.2.0]: https://gitlab.com/booteille/invidition/compare/dfaaa962...c72a71bc
[0.1.0]: https://gitlab.com/booteille/invidition/compare/a479fcb3...dfaaa962
