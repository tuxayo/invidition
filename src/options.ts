import config from './lib/config.ts';
import * as browser from 'webextension-polyfill';
import * as _ from 'lodash';
import ISO6391 from 'iso-639-1';
import { MessageType } from './lib/types';
import * as helpers from './lib/helpers';

// When saving options
function saveOptions(e) {
    e.preventDefault();

    let options = {};
    for (var option in config.options) {
        if (option !== 'subtitles') {
            let element: Element = document.getElementById(option);
            let inputElement: HTMLInputElement = element as HTMLInputElement;

            if (inputElement.type !== 'undefined' && inputElement.type === 'checkbox') {
                if (option === 'autoplay') {
                    options[option] = inputElement.checked ? '1' : '0';
                } else {
                    options[option] = `${inputElement.checked}`;
                }
            } else {
                options[option] = inputElement.value;
            }
        } else {
            options['subtitles'] = captionsQuery();
        }
    }

    // Save settings
    browser.storage.sync.set(options);

    // Reload Options
    let sending = browser.runtime.sendMessage({ type: MessageType.OptionsUpdated });
}

// When options page is loaded
function restoreOptions() {
    function setCurrentChoice(result) {
        for (const option in config.options) {
            if (option !== 'subtitles') {
                let element: Element = document.getElementById(option);
                let value = result && result[option] ? result[option] : config.options[option];
                let htmlElement: HTMLElement = element as HTMLElement;

                switch (htmlElement.tagName) {
                    case 'INPUT':
                        let inputElement: HTMLInputElement = htmlElement as HTMLInputElement;

                        if (inputElement.type === 'checkbox') {
                            if (option === 'autoplay') {
                                inputElement.checked = value === '1';
                            } else {
                                inputElement.checked = value === 'true';
                            }
                        } else {
                            inputElement.value = value;
                        }
                        break;
                    case 'SELECT':
                        (<HTMLSelectElement>htmlElement).value = value;
                        break;
                }
            } else {
                let captions = _.split(result[option], ',');
                let selectElements = document.getElementsByClassName('defaultCaptions');

                let i = 0;
                for (i; i < selectElements.length; i++) {
                    (selectElements[i] as HTMLOptionElement).value = captions[i];
                }
            }
        }
    }

    function onError(error) {
        console.error(`Error: ${error}`);
    }

    // Generate Locales DropDowns
    let defaultCaptions = document.getElementsByClassName('defaultCaptions');

    _.forEach(defaultCaptions, (element) => {
        let defaultOption: HTMLOptionElement = document.createElement("OPTION") as HTMLOptionElement;
        defaultOption.value = "";
        defaultOption.selected = true;

        element.append(defaultOption);

        _.forEach(ISO6391.getAllCodes(), (code) => {
            let option: HTMLOptionElement = document.createElement("OPTION") as HTMLOptionElement;
            option.value = code;
            option.innerText = ISO6391.getNativeName(code);

            element.append(option);
        })
    });

    let getting = browser.storage.sync.get();
    getting.then(setCurrentChoice, onError);
}

const captionsQuery = (): string => {
    let defaultCaptions = document.getElementsByClassName('defaultCaptions');
    let query = '';

    _.forEach(defaultCaptions, (element: HTMLOptionElement) => {
        query += `${element.value || ''},`
    });

    return query.substr(0, query.length - 1);
}

const insertI18nIntoDocument = () => {
    helpers.insertI18nTitleIntoDocument(document);
    helpers.insertI18nContentIntoDocument(document);
}

document.addEventListener("DOMContentLoaded", () => {
    // Generate available languages
    _.forEach(config.availableLanguages, (languageTag) => {
        if (browser.i18n.getUILanguage() === helpers.normalizeLanguageCodeToBrowser(languageTag)) {
            config.options.hl = languageTag;
        }
    });

    _.forEach(config.availableLanguages, (languageTag) => {
        let el = document.createElement("OPTION") as HTMLOptionElement;
        el.value = languageTag;
        el.innerText = helpers.trimLocaleSubtag(languageTag);
        document.getElementById('hl').append(el);
    });
})
document.addEventListener("DOMContentLoaded", insertI18nIntoDocument);
document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
