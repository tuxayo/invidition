import * as browser from 'webextension-polyfill';
import config from './lib/config';
import * as _ from 'lodash';
import { MessageType } from './lib/types';
import * as helpers from './lib/helpers';

let tabsToReload = [];
let options: any = {};

const invidition = async (r) => {
    // If extension is disabled, do not redirect
    if (!config.isEnabled) {
        return {};
    }

    let url = new URL(r.url);

    // Have to workaround Iframe API
    if (url.pathname === '/iframe_api') {
        url.href = browser.runtime.getURL('/assets/js/youtube/iframe_api.js');
    } else {
        if (url.pathname.indexOf('www-widgetapi.js') != -1) {
            url.href = browser.runtime.getURL('/assets/js/youtube/www-widgetapi.js');
        } else {
            // If not iFrame API files, redirect to invidio.us
            let queryChanged = false;

            let instance = new URL(<string>options.instance);
            if (url.hostname.indexOf(instance.hostname) === -1) {
                queryChanged = true;

                url.hostname = instance.hostname;

                // Force HTTP if instance is using Tor. Otherwise, force HTTPS.
                url.protocol = _.endsWith(url.hostname, '.onion') ? 'http' : 'https';
            }

            // Set up the interface language
            if (!url.searchParams.get('hl') || url.searchParams.get('hl') !== options.hl) {
                queryChanged = true;

                url.searchParams.set('hl', options.hl);
            }

            if (_.startsWith(url.pathname, '/embed') || _.startsWith(url.pathname, '/watch')) {
                _.forEach(options, (value, key) => {
                    if (key !== 'instance') {
                        if (url.searchParams.get('continue')) {
                            if (key === 'autoplay_next') {
                                let autoplay = (value === 'true') ? '1' :
                                    (options.autoplay === '1') ? '1' :
                                        '0';
                                if (autoplay !== url.searchParams.get(key)) {
                                    url.searchParams.set('autoplay', autoplay);
                                }

                                return;
                            } else if (key === 'autoplay') {
                                return;
                            }
                        } else {
                            if (key === 'autoplay_next') {
                                return;
                            }
                        }


                        if (!url.searchParams.get(key) || url.searchParams.get(key) !== value) {
                            queryChanged = true;

                            url.searchParams.set(key, value);
                        }
                    }
                });
            }

            if (!queryChanged) {
                return {};
            }
        }
    }

    console.log(url.href);
    return {
        redirectUrl: url.href
    };
}

const toggleEnabled = async () => {
    config.isEnabled = !config.isEnabled;
    let iconsPaths = {};
    let action = "";
    tabsToReload = [];

    _.forEach(await browser.tabs.query({}), (tab) => {
        tabsToReload.push(tab.id);
    });

    if (config.isEnabled) {
        iconsPaths = generateIconsPaths();
        action = "Deactivate";

        reloadTab();
    } else {
        iconsPaths = generateIconsPaths(true);
        action = "Activate"
    }

    browser.browserAction.setTitle({ title: `${action} Invidious` })
    browser.browserAction.setIcon({
        path: iconsPaths
    });
}

const reloadTab = async (id = null) => {
    let activeTab = (await browser.tabs.query({ currentWindow: true, active: true }))[0];
    let activeTabHostname = new URL(activeTab.url).hostname;

    if (tabsToReload.indexOf(activeTab.id) !== -1 && generateSubdomain().indexOf(activeTabHostname) !== -1) {
        tabsToReload = _.without(tabsToReload, activeTab.id);

        browser.tabs.reload(id);
    }
}

const handleTabActivation = async (tab) => {
    if (config.isEnabled) {
        reloadTab();
    }
}

const loadOptions = async () => {
    _.forEach(config.availableLanguages, (languageTag) => {
        if (browser.i18n.getUILanguage() === helpers.normalizeLanguageCodeToBrowser(languageTag)) {
            config.options.hl = languageTag;
        }
    });

    const opts = await browser.storage.sync.get();

    _.forEach(config.options, (value, key) => {
        options[key] = opts[key] || config.options[key];
    })
}

const generateSubdomain = () => {
    let domains: string[] = [];

    _.forEach(config.domains.toRedirect, (value) => {
        domains.push(value);
        domains.push(`www.${value}`);
    })

    const instance = new URL(options.instance);

    domains.push(instance.hostname);
    domains.push(`www.${instance.hostname}`);

    return domains;
}

const generateIconsPaths = (disabled = false) => {
    let paths = {};

    _.forEach(config.icons.resolutions, (value) => {
        paths[value] = `/assets/img/logo-${value}${disabled ? '-disabled' : ''}.png`;
    });

    return paths;
};

const generateFilter = async () => {
    let urls: string[] = [];
    await loadOptions();

    _.forEach(config.domains.toRedirect, (value) => {
        urls.push(`*://*.${value}/*`);
    })

    urls.push(`*://*.${(new URL(options.instance)).hostname}/*`);

    return urls;
};

const reloadInviditionListener = async () => {
    await loadOptions();
    browser.webRequest.onBeforeRequest.removeListener(invidition);
    browser.webRequest.onBeforeRequest.addListener(
        invidition,
        { urls: await generateFilter() },
        ['blocking']
    );
};

reloadInviditionListener();

browser.browserAction.onClicked.addListener(toggleEnabled);
browser.tabs.onActivated.addListener(handleTabActivation);
browser.runtime.onMessage.addListener((message) => {
    switch (message.type) {
        case MessageType.OptionsUpdated:
            return reloadInviditionListener();
    }
});
