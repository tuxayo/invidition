import * as browser from 'webextension-polyfill';

export default {
    isEnabled: true,
    options: {
        instance: "https://invidio.us/",
        hl: 'en-US',
        local: 'true',
        autoplay: '0',
        subtitles: ',,',
        autoplay_next: true
    },
    domains: {
        toRedirect: [
            'youtube.com',
            'youtube-nocookie.com',
            'youtu.be',
            's.ytimg.com'
        ]
    },
    availableLanguages: [
        'ar',
        'de',
        'en-US',
        'es',
        'eu',
        'fr',
        'it',
        'nb_NO',
        'nl',
        'pl',
        'ru'
    ],
    icons: {
        resolutions: [16, 32, 48, 64, 96, 128]
    }
};
