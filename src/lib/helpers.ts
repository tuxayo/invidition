import * as _ from 'lodash';
import ISO6391 from 'iso-639-1';
import * as browser from 'webextension-polyfill';

export const insertI18nContentIntoDocument = (document) => {
    let elements = document.querySelectorAll('[data-i18n-content]');

    _.forEach(elements, (el) => {
        let messageName = el.getAttribute('data-i18n-content');

        el.innerText = browser.i18n.getMessage(messageName);
    });
};

export const insertI18nTitleIntoDocument = (document) => {
    let elements = document.querySelectorAll('[data-i18n-title]');

    _.forEach(elements, (el) => {
        let messageName = el.getAttribute('data-i18n-title');

        el.setAttribute('title', browser.i18n.getMessage(messageName));
    });
};

export const normalizeLanguageCodeToBrowser = (code) => {
    return _.replace(code, '_', '-');
}

export const trimLocaleSubtag = (code) => {
    return ISO6391.getNativeName(code.substr(0, 2));
}
