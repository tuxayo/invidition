const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: {
        background: 'background.ts',
        options: ['options.html', 'options.ts'],
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
            name: 'vendors'
        }
    },
    output: {
        path: path.join(__dirname, 'extension', 'dist'),
        filename: '[name].js',
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.ts', '.tsx'],
        modules: [
            path.join(__dirname, 'src'),
            'node_modules',
        ],
    },
    module: {
        rules: [{
                test: /\.html$/,
                use: [{
                        loader: 'file-loader',
                        options: {
                            context: 'src',
                            name: '[path][name].[ext]',
                        },
                    },
                    'extract-loader',
                    'html-loader',
                ],
            },
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'ts-loader',
                },
            },
        ],
    },
    plugins: [
        // Since some NodeJS modules expect to be running in Node, it is helpful
        // to set this environment var to avoid reference errors.
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production'),
        }),
    ],
    // This will expose source map files so that errors will point to your
    // original source files instead of the transpiled files.
    devtool: 'sourcemap',
};
